#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <string>
#include <math.h>

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
	Mat imgS, imgD, imgPro;

	imgS = imread("input.jpg", CV_LOAD_IMAGE_GRAYSCALE); //Åª¨ú¹Ï¹³BGR

	Mat imgX(imgS.rows, imgS.cols, CV_8U, Scalar(0));
	Mat imgY(imgS.rows, imgS.cols, CV_8U, Scalar(0));

	Sobel(imgS, imgX, CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT);
	convertScaleAbs(imgX, imgX);
	Sobel(imgS, imgY, CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT);
	convertScaleAbs(imgY, imgY);

	imwrite("imgX.jpg ", imgX);
	imwrite("imgY.jpg ", imgY);

	addWeighted(imgX, 0.8, imgY, 0.8, 0, imgD);

	imwrite("imgD.jpg ", imgD);

	//imwrite("imgPro.jpg ", imgPro);

	waitKey(0);

	return 0;
}